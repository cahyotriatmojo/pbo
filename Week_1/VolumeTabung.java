public class VolumeTabung {
 public static void main(String[] args) {
  // DIAMETER 5 JADI R = 2,5

  double volume, r, phi = 3.14;
  int t;

  r = 2.5;
  t = 10;
  volume = (phi * r * r) * t;
  System.out.println("Volume  Tabung : " + volume);
 }

}
