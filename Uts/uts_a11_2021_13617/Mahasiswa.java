package uts_a11_2021_13617;

public class Mahasiswa {
    String nim;
	   String nama;
    int semester;
    int usia;
	   String krs[];

	public Mahasiswa(String nim, String nama, int semester, int usia, String krs[]) {
		this.nim = nim;
		this.nama = nama;
		this.semester = semester;
		this.usia = usia;
		this.krs = krs;
	}

	void infoMahasiswa(){
		System.out.println("  Hasil Cetak  ");
		System.out.println("Nama "+nama);
		System.out.println("Nim "+nim);
		System.out.println("Semester: "+semester);
		System.out.println("Usia: "+usia);
	}

	float hitungRataNilai(int nilai[]){

		float hasil;
		int total = 0;

		for(int i = 0;i < nilai.length; i++) {
			System.out.print("Masukkan Nilai KRS Ke-"+(i+1)+": ");
			total = total + nilai[i];
		}

		hasil = (float) total / nilai.length;

		return hasil;
	}

	void infoKRSMahasiswa(){
		int krsl = krs.length;

		for (int i = 0; i < krsl; i++) {
			System.out.println("");
			System.out.println((i+1) + ". "+krs[i]);
		}
	}

}
