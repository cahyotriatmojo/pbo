package uts_a11_2021_13617;
public class MahasiswaLulus extends Mahasiswa {

 float ipk;
 int tahunwisuda;

 public MahasiswaLulus(String nim, String nama, int semester, int usia, String krs[], float ipk, int tahunwisuda){
  super(nim, nama, semester, usia, krs);
  this.tahunwisuda = tahunwisuda;
  this.ipk = ipk;
 }

 boolean ikutWisuda(){
  boolean wisuda = true;

  return wisuda;
 }

 @Override
 void infoMahasiswa() {
  super.infoMahasiswa();
  System.out.println("IPK: "+ipk);
  System.out.println("Tahun Wisuda: "+tahunwisuda);
 }

}
