package uts_a11_2021_13617;
import java.util.ArrayList;
import java.util.Scanner;

public class Main{



	public static void main(String[] args) {

		//mendeklarasikan Struktur ArrayList
		ArrayList<MahasiswaBaru> baru = new ArrayList<>();
		ArrayList<MahasiswaAktif> aktif = new ArrayList<>();
		ArrayList<MahasiswaLulus> lulus = new ArrayList<>();
		ArrayList<MahasiswaTransfer> transfer = new ArrayList<>();

		Scanner inp = new Scanner(System.in);

		int pilih;
		do{
			System.out.println("1. Mahasiswa Baru");
			System.out.println("2. Mahasiswa Lulus");
			System.out.println("3. Mahasiswa Transfer");
			System.out.println("4. Mahasiswa Aktif");
			System.out.println("5. Keluar");

			System.out.print("Masukkan pilihan: ");
			pilih = inp.nextInt();

			switch (pilih) {
				case 1:
						System.out.print("Masukkan jumlah Mahasiswa Baru: ");
						int n1 = inp.nextInt();

								for (int i = 0; i < n1; i++) { //perulangan inputan yang dicetak...
												System.out.println("Data Mahasiswa Ke-" + (i+1));

												System.out.print("Nama: ");
												String nama = inp.next();
												System.out.print("Nim: ");
												String nim = inp.next();
												System.out.print("Semester: ");
												int semester = inp.nextInt();
												System.out.print("Usia: ");
												int usia = inp.nextInt();
												System.out.print("Asal Sekolah: ");
												String asalSekolah = inp.next();
												System.out.print("\n");

												System.out.print("Jumlah KRS yang ditempuh: ");
												int rs = inp.nextInt();

												String[] krs = new String[rs];
												for (int j = 0; j < rs; j++) {  //perulangan inputan berapa jmlh KRS...
													System.out.print("Nama KRS Ke-"+ (j+1)+ ":");
													krs[j] = inp.next();
												}

												//buat objek mhsBaru dari class MahasiswaBaru
												MahasiswaBaru mhsBaru = new MahasiswaBaru(nim, nama, semester, usia, krs, asalSekolah);
												baru.add(mhsBaru);   //baru dari objek baru arraylist yg dibuat di atas...

								}

								System.out.println("\nDaftar Mahasiswa Baru");
								for(MahasiswaBaru mhsBaru : baru){
									mhsBaru.infoMahasiswa();
									System.out.print("\nIkut serta Ospek: "+mhsBaru.ikutOspek());
									System.out.println("\n");
								}
					 break;

				case 2:
				System.out.print("Masukkan Jumlah Mahasiswa Lulus: ");
								int n4 = inp.nextInt();

								for (int i = 0; i < n4; i++) {
									System.out.println("Data Mahasiswa Ke-" + (i+1));

									 System.out.print("Nama: ");
										String nama = inp.next();
										System.out.print("Nim: ");
										String nim = inp.next();
						 		 System.out.print("Semester: ");
						 		 int semester = inp.nextInt();
						 		 System.out.print("Usia: ");
						 		 int usia = inp.nextInt();
										System.out.print("Tahun Wisuda: ");
										int tahunWisuda = inp.nextInt();
										System.out.print("IPK: ");
										float ipk = inp.nextFloat();
										System.out.print("\n");

										System.out.print("Jumlah KRS yang ditempuh: ");
										int rs = inp.nextInt();

										String[] krs = new String[rs];

										MahasiswaLulus mhsLulus = new MahasiswaLulus(nim, nama, semester, usia, krs, ipk, tahunWisuda);

										//buat method add()
										lulus.add(mhsLulus);  //lulus dari objek yang dibuat pada class ArrayList
								}

						System.out.print("\nDaftar Mahasiswa Lulus");
						for(MahasiswaLulus mhsLulus : lulus){  //perulangan data telah diinputkan pada MahasiswaLulus
										mhsLulus.infoMahasiswa();
										System.out.print("\nIkut serta Ospek: "+mhsLulus.ikutWisuda());
										System.out.println("\n");
						}
					 break;

				case 3:
				    System.out.print("Masukkan jumlah Mahasiswa Transfer: ");
				    int n3 = inp.nextInt();

						  for (int i = 0; i < n3; i++) {
										System.out.println("Data Mahasiswa Ke-" + (i+1) + ": ");

									 System.out.print("Nama: ");
										String nama = inp.next();
										System.out.print("Nim: ");
										String nim = inp.next();
						 		 System.out.print("Semester: ");
						 		 int semester = inp.nextInt();
						 		 System.out.print("Usia: ");
						 		 int usia = inp.nextInt();
										System.out.print("Asal Sekolah: ");
										String asalSekolah = inp.next();
										System.out.print("Asal Universitas: ");
										String asalUniversitas = inp.next();
										System.out.print("\n");

										System.out.print("Jumlah KRS yang ditempuh: ");
										int rs = inp.nextInt();

										String[] krs = new String[rs];
										for (int j = 0; j < rs; j++) {
											System.out.print("Nama KRS Ke-"+ (j+1)+ ":");
											krs[j] = inp.next();
										}

										MahasiswaTransfer mhsTransfer = new MahasiswaTransfer(nim, nama, semester, usia, krs, asalSekolah, asalUniversitas);
										transfer.add(mhsTransfer); //add(mhsTransfer) berguna utk menambah data baru ke dalam array list mhsTransfer...
						  }

							System.out.println("\nDaftar Mahasiswa Transfer");
							for(MahasiswaTransfer mhsTransfer : transfer){
							 	mhsTransfer.infoMahasiswa();
									System.out.print("\nIkut serta Ospek: "+mhsTransfer.ikutOspek());
									System.out.println("\nInfo KRS");
									mhsTransfer.infoKRSMahasiswa();
									System.out.println("\n");
							}
					 break;

				case 4:
						float vle;
						int[] nilai;  //menginputkan nilai panjang array dan menjadi Parameter pada hitung rata nilai

								System.out.print("Nama: ");
								String nama = inp.next();
								System.out.print("Nim: ");
								String nim = inp.next();
								System.out.print("Semester: ");
								int semester = inp.nextInt();
								System.out.print("Usia: ");
								int usia = inp.nextInt();
								System.out.print("\n");

								System.out.print("Masukkan jumlah KRS yang ditempuh: ");
								int rs = inp.nextInt();

								String[] krs = new String[rs];
								for (int i = 0; i < rs; i++) {
									System.out.print("Masukkan Nama Matkul ke-"+(i+1) +": ");
									krs[i] = inp.next();
								}

								//array nilai dengan panjang yg di inputkan pada int rs
								nilai = new int[rs];

								//Input nilai krs pada array menggunakan looping
								for (int j = 0; j < rs; j++) {
									System.out.print("Masukkan Nilai KRS ke-" + (j+1) +": ");
									nilai[j] = inp.nextInt();
								}

								//Bikin objek
								MahasiswaAktif mhsAktif = new MahasiswaAktif(nim, nama, semester, usia, krs);

								//vle akan menampung fungsi dari hitungRataNilai dengan parameter (nilai)
								vle = mhsAktif.hitungRataNilai(nilai);

								//Menambahkan objek MahasiswaAktif ke dalam ArrayList
								aktif.add(mhsAktif);   //aktif yaitu objek baru ArrayList

						//Mencetak hasil MahasiswaAktif
					 for (MahasiswaAktif Aktif : aktif){
						System.out.print("\nDaftar Mahasiswa Aktif");
										Aktif.infoMahasiswa();
										Aktif.infoKRSMahasiswa();
										System.out.print("Rata - Rata Nilai: "+vle);
										System.out.println("\n");
						}
					 break;
	//System.out.print("\nInfo KRS");
				case 5:
				      System.out.println("Anda Keluar dari Program");
					 break;

				default:
										System.out.println("Pilihan Tidak Tersedia");
					 break;
			}

		} while (pilih != 5);

		//inp.close();
	}

}
		//inp.close();
	// 	int usia, semester;
	// 	String nim, nama;
	// 	boolean Ospek = false;
	// 	boolean Wisuda = false;

	// 	System.out.print("Masukkan Jumlah KRS saat ini: ");
	// 	Scanner inp = new Scanner(System.in);
	// 	n = inp.nextInt();

	// 	String[] krs = new String[n];

	// 	 for(int i = 0;i < n;i++) {
	// 		 System.out.println("KRS ke-"+(i+1));
	// 		 System.out.print("Nama KRS yang di tempuh: ");
	// 		 krs[i] = inp.next();
	// 	 }

	// 		System.out.print("Masukkan NIM: ");
	// 		nim = inp.next();
	// 		System.out.print("Masukkan Nama: ");
	// 		nama = inp.next();
	// 		System.out.print("Masukkan Usia: ");
	// 		usia = inp.nextInt();
	// 		System.out.print("Masukkan Semester: ");
	// 		semester = inp.nextInt();

	// 		//System.out.println(" Hasil Inputan \n");

	// 		Mahasiswa mhs = new Mahasiswa(nim, nama, semester, usia, krs);
	// 	 MahasiswaBaru baru = new MahasiswaBaru(nim, nama, semester, usia, krs, "SMA N 2 Semarang");
	// 		MahasiswaTransfer trnfer = new MahasiswaTransfer(nim, nama, semester, usia, krs, "BINUS");
	// 		MahasiswaLulus lls = new MahasiswaLulus(nim, nama, semester, usia, krs, 37, 2022);

	// 		//mhs.infoMahasiswa();
	// 		//mhs.infoKRSMahasiswa();
	// 		mhs.hitungRataNilai(n);

	// 		baru.infoMahasiswa();
	// 		Ospek = baru.ikutOspek();
	// 		if(Ospek){
	// 			System.out.println(baru.nama+" Ikut Ospek");
	// 		}

	// 		trnfer.infoMahasiswa();
	// 		trnfer.ikutOspek();

	// 		lls.infoMahasiswa();
	// 		Wisuda = lls.ikutWisuda();
	// 		if(Wisuda){
	// 			System.out.println(lls.tahunwisuda+" Mengikuti Wisuda");
	// 		}

	// 	 setMahasiswa(nim,nama, usia, semester, krs);
	// 		inp.close();
 // }

 //  	public static void setMahasiswa(String nim, String nama, int usia, int semester, String krs[]) {
	// 				ArrayList<Mahasiswa> arrayListmhs = new ArrayList<>();
	// 					for(int i=0; i < n; i++) {
	// 					 arrayListmhs.add(new Mahasiswa(nim,nama,usia,semester,krs));  //krs[i]
	// 	 			}
	// 					cetakMahasiswa(arrayListmhs);
	// 		}

	// 		public static void cetakMahasiswa(ArrayList<Mahasiswa> list){
	// 			for (int i = 0; i < n; i++) {
	// 				Mahasiswa arrayListmhs = list.get(i);
	// 				System.out.println("Ipk " +arrayListmhs.krs);
	// 			}



