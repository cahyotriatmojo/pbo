import java.lang.Math;
import java.util.Scanner;

public class Cellphone implements Phone {

  String merk;
  String type;
  int batteryLevel;
  int status;
  int volume = (int) (Math.random() * (100 - 0 + 1) + 0);
  int pulsa = 0;
  int pilih;

  public Cellphone(String merk, String type) {
    this.merk = merk;
    this.type = type;
    this.batteryLevel = (int) (Math.random() * (100 - 0 + 1) + 0);
  }

  public void powerOn() {
    this.status = 1;
    System.out.println("Ponsel menyala");
  }

  public void powerOff() {
    this.status = 0;
    System.out.println("Ponsel mati");
  }

  public void volumeUp() {
    if (this.status == 0) {
      System.out.println("Ponsel mati. Tidak dapat menaikkan volume\n");
    } else {
      this.volume++;
      if (this.volume >= 100) {
        this.volume = 100;
      }
      System.out.println("Volume: " + getVolume() + "\n");
    }
  }

  public void volumeDown() {
    if (this.status == 0) {
      System.out.println("Ponsel mati. Tidak dapat menurunkan volume\n");
    } else {
      this.volume--;
      if (this.volume <= 0) {
        this.volume = 0;
      }
      System.out.println("Volume: " + getVolume() + "\n");
    }
  }

  public int getVolume() {
    if (this.status == 0) {
      System.out.println("Ponsel mati. Tidak dapat memeriksa volume\n");
      return 0;
    } else {
      return this.volume;
    }
  }

  public void topupPulsa() {
    if (this.status == 0) {
      System.out.println("Ponsel mati. Tidak dapat melakukan Top Up Pulsa\n");
    } else {
      System.out.println("Pilih Jumlah Pulsa:");
      System.out.println("1. Rp.5.000");
      System.out.println("2. Rp.15.000");
      System.out.println("3. Rp.25.000");
      System.out.println("4. Rp.50.000");
      System.out.println("5. Rp.100.000");

      Scanner inSaldo = new Scanner(System.in);

      System.out.print("Masukkan Pilihan: ");
      pilih = inSaldo.nextInt();

      switch (pilih) {
        case 1:
          this.pulsa += 5000;
          System.out.println("Top Up sejumlah " + this.pulsa + " telah berhasil!\n");
          break;
        case 2:
          this.pulsa += 15000;
          System.out.println("Top Up sejumlah " + this.pulsa + " telah berhasil!\n");
          break;
        case 3:
          this.pulsa += 25000;
          System.out.println("Top Up sejumlah " + this.pulsa + " telah berhasil!\n");
          break;
        case 4:
          this.pulsa += 50000;
          System.out.println("Top Up sejumlah " + this.pulsa + " telah berhasil!\n");
          break;
        case 5:
          this.pulsa += 100000;
          System.out.println("Top Up sejumlah " + this.pulsa + " telah berhasil!\n");
          break;
        default:
          System.out.println("Pilihan tidak terdaftar!");
          break;
      }
    }
  }

  public void saldoPulsa() {
    if (this.status == 0) {
      System.out.println("Ponsel mati. Tidak dapat mengecek Pulsa\n");
    } else {
      System.out.println("Sisa pulsa anda: " + this.pulsa);
    }
  }

}
