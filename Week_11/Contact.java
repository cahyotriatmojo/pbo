import java.util.ArrayList;
import java.util.Scanner;

public class Contact {

  String nama;
  String nomor;
  int status;

  ArrayList<Contact> contact = new ArrayList<>();
  Scanner inKontak = new Scanner(System.in);

  public Contact(String nama, String nomor) {
    this.nama = nama;
    this.nomor = nomor;
  }

  String getNama() {

    System.out.println("Nama: " + nama);
    return this.nama;
  }

  String getNomor() {

    System.out.println("Nomer: " + nomor);
    return this.nomor;
  }

  public void setStatusContact(int status) {
    this.status = status;
  }

  public void tambahKontak() {
    if (status == 0) {
      System.out.println("Ponsel mati. Tidak dapat menambah Kontak\n");
    } else {

      System.out.print("Nama: ");
      nama = inKontak.next();
      System.out.print("Nomor: ");
      nomor = inKontak.next();

      Contact cont = new Contact(nama, nomor);
      contact.add(cont);

      System.out.print("Kontak Berhasil Ditambah\n");
    }
  }

  public void tampilKontak() {
    if (status == 0) {
      System.out.println("Ponsel mati. Tidak dapat menampilkan Kontak\n");
    } else {
      for (int i = 0; i < contact.size(); i++) {
        Contact cont = contact.get(i);
        System.out.println(cont.getNama() + " - " + cont.getNomor());
      }
      System.out.print("\n");
    }
  }

  public void mencariKontak() {
    if (status == 0) {
      System.out.println("Ponsel mati. Tidak dapat mencari Kontak\n");
    } else {

      String nama;

      System.out.print("Nama Kontak: ");
      nama = inKontak.next();

      boolean search = contact.stream().anyMatch(value -> value.getNama().equals(nama));
      if (search) {
        for (Contact cont : contact) {
          if (cont.nama.equals(nama)) {
            System.out.println("Kontak Berhasil Ditemukan => " + cont.getNama() + " " + cont.getNomor());
          }
        }
      } else {
        System.out.println("Tidak Ditemukan");
      }
    }
  }

}

