import java.util.Scanner;

public class Cellphonemain {
    public static void main(String[] args){

        Cellphone cell = new Cellphone("Nokia", "3310"); 

        Contact cont = new Contact(null, null); 

        Scanner input = new Scanner(System.in);

        int pilih;
        do {
            System.out.println("\n");
            System.out.println("1. Hidupkan Phone");
            System.out.println("2. Matikan Phone");
            System.out.println("3. Top UP Pulsa");
            System.out.println("4. Melihat Saldo Pulsa");
            System.out.println("5. Tambah Contact");
            System.out.println("6. Melihat isi Contact");
            System.out.println("7. Mencari Contact");
            System.out.println("8. Menaikan Volume");
            System.out.println("9. Menurunkan Volume");
            

            System.out.print("Masukkan Pilihan: ");
            pilih = input.nextInt();

            System.out.println("\n");

            switch (pilih) {
               
                case 1:

                    cont.setStatusContact(pilih); 
                    cell.powerOn();
                    break;

                case 2:

                    cont.setStatusContact(pilih); 
                    cell.powerOff();
                    break;

                case 3:

                    cell.topupPulsa();
                    break;

                case 4:

                    cell.saldoPulsa();
                    break;

                case 5:
            
                    cont.tambahKontak();               
                    break;

                case 6:

                    cont.tampilKontak();
                    break;

                case 7:

                    cont.mencariKontak();
                    break;
                case 8:

                    cell.volumeDown();
                    break;
                case 9:

                    cell.volumeUp();
                    break;
                 
                default:
                    System.out.println("Pilihan Tidak Ada");
                    break;
            }
            
        } while (pilih != 10);
       
    }

}
