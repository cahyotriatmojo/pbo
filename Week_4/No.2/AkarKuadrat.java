import java.util.Scanner;


public class AkarKuadrat {


    int a, b, c, D;
    double x1, x2;

    public void Data(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;

        D = (b*b) - (4*a*c);
        System.out.print("Determinan: " + D);
    }

    public void Penjumlahan() {
        if (D > 0) {
            System.out.println(" (Akar Real DAN Berbeda atau Berlainan)");
            x1 = (-b + Math.sqrt(D)) / (2*a);
            //DAN
            x2 = (-b - Math.sqrt(D)) / (2*a);

            System.out.println("X1 = " +x1);
            System.out.println("X2 = " +x2);
        }

        else if(D == 0){
            System.out.println(" (Akar Real DAN Sama/Kembar)");
            x1 = x2 = -b /(2*a);

            System.out.println("X1 = " +x1);
            System.out.println("X2 = " +x2);
        }

        else{
            System.out.println(" (Tidak Real/Akar Imajiner)");

            x1 = -b/(2*a) + (Math.sqrt(- D))/(2*a);
            x2 = -b/(2*a) - (Math.sqrt(- D))/(2*a);

            System.out.println("X1 = " +x1+ " DAN " +x2);
        }

    }
    public static void main(String[] args) throws Exception {

        char ulang;
      do{

        Scanner input = new Scanner(System.in);

        System.out.print("Input nilai a: ");
        int a = input.nextInt();

        System.out.print("Input nilai b: ");
        int b = input.nextInt();

        System.out.print("Input nilai c: ");
        int c = input.nextInt();

        AkarKuadrat abc = new AkarKuadrat();
        abc.Data(a, b, c);
        abc.Penjumlahan();

        Scanner data = new Scanner(System.in);
        System.out.println("\nInput Data Lagi? [Y/T]: ");

        ulang = data.next().charAt(0);
        System.out.print("\n");

        } while (ulang!='T');

    }

}
