import java.util.Scanner;

public class Penjualan {

    public static void main(String[] args) throws Exception {

        boolean cek = true;  //buat cek while

        do {
            Scanner input = new Scanner(System.in);

            System.out.print("Masukan Kode Barang: ");
            String kode = input.nextLine();
            System.out.print("Masukan Nama Barang: ");
            String nama = input.nextLine(); // BUAT NAMPUNG INPUTAN DGN VAR INI
            System.out.print("Masukan Harga Barang: "); // PROSES INPUT
            float harga = input.nextFloat();
            System.out.print("Masukan Jumlah Barang: ");
            int jumlah = input.nextInt();



            Nilai jual1 = new Nilai(); //objek baru jual1  //Nilai dari class yg kita ingin ambil

            float totHarga = jual1.getTotalBarang(harga, jumlah);

            String bonus = jual1.getBonus(totHarga, jumlah);

            System.out.println("\n \n \n \n ");

            jual1.cetakNota(kode, nama, harga, jumlah, totHarga, bonus);


            System.out.println("\n \n ");

            System.out.print("Apakah ingin input lagi? [y/t]");

            char lagi = input.next().charAt(0); // INPUTAN DGN VARIABEL NAMA lagi

            if (lagi == 't') {
                cek = false;
            }

        } while (cek);

    }
}
