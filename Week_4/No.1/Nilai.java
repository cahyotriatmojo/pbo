public class Nilai {
    // Deklarasi Atribut
    String kode, nama, bonus;
    float harga, total;
    int jumlah;

    // Deklarasi Method
    void setData(String kode, String nama, float harga, int jumlah) {
        this.kode = kode;
        this.nama = nama;
        this.harga = harga;
        this.jumlah = jumlah;
    }

    float getTotalBarang(float harga, int jumlah) {
        total = harga * jumlah;

        return total;
    }

    String getBonus(float total, int jumlah) {
        if (total >= 500000 && jumlah > 5) {
            bonus = "Setrika";
        } else if (total >= 100000 && jumlah > 3) {
            bonus = "Payung";
        } else if (total >= 50000 || jumlah > 2) {
            bonus = "Bolpoint";
        } else {
            bonus = "Tidak Ada Bonus";
        }
        return bonus;
    }

    void cetakNota(String kode, String nama, float harga, int jumlah, float total, String bonus) {
        this.kode = kode;
        this.nama = nama;
        this.harga = harga;
        this.jumlah = jumlah;
        this.total = total;
        this.bonus = bonus;

        String hrg = String.format(" Harga           : %.2f", harga).replaceAll("0*$", "").replaceAll("\\.$", "");
        String ttl = String.format(" Total Harga     : %.2f", total).replaceAll("0*$", "").replaceAll("\\.$", "");

        System.out.println(" BERIKUT ADALAH NOTA ANDA ");
        System.out.println(" Kode Barang     : " + kode);
        System.out.println(" Nama Barang     : " + nama);
        System.out.println(hrg);
        System.out.println(" Jumlah Barang   : " + jumlah);
        System.out.println(ttl);
        System.out.println(" Bonus Barang    : " + bonus);

    }
}
