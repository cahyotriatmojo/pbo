import java.util.Scanner;

public class Waktu {

    public static void main(String[] args) throws Exception {

        /**
         * System.out.println("Hari " +180061/86400);
         * System.out.println((180061%86400));
         * System.out.println("Jam "+(180061%86400)/3600);
         * System.out.println((180061%86400)%3600);
         * System.out.println("Menit "+((180061%86400)%3600)/60);
         * System.out.println("Detik "+ ((180061%86400)%3600)%60);
         *
         */
        boolean cek = true;
        do {
            Scanner input = new Scanner(System.in);

            System.out.print("Masukan Detik : ");
            int dtk = input.nextInt();

            Jumlah konvers = new Jumlah();

            int Hari = konvers.getHari(dtk);
            int Jam = konvers.getJam(dtk);
            int Menit = konvers.getMenit(dtk);
            int Detik = konvers.getDetik(dtk);

            System.out.println(" HASIL KONVERSI");
            System.out.println("Hari = " + Hari);
            System.out.println("Jam = " + Jam);
            System.out.println("Menit = " + Menit);
            System.out.println("Detik = " + Detik);

            System.out.println("\n \n ");
            System.out.print("Apakah ingin input lagi? [y/t]");

            char lagi = input.next().charAt(0);

            if (lagi == 'y') {
                cek = false;
            }

        } while (cek);

    }
}