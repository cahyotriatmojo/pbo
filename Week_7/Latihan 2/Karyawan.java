public class Karyawan {

 String nama;
 int umur;
 int tunjanganAnak;

 public Karyawan(String nama, int umur, int tunjanganAnak) { // kontrutor dengan isi an parameter
  this.nama = nama;
  this.umur = umur;
  this.tunjanganAnak = tunjanganAnak;
 }

 public void cetak() { // method void cetak()
  System.out.println(" \nNama: " + nama);
  System.out.println("Umur: " + umur);
 }
}
