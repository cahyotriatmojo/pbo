import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {

        int anak;
        int pokok;

        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan Jumlah Tunjangan Anak: ");
        anak = input.nextInt(); // input jmlh tunjangan Anak

        System.out.print("Masukan Jumlah Gaji Pokok: ");
        pokok = input.nextInt(); //input jmlh gaji pokok

        // untuk Karyawan kontrak..upah dan kehadiran saya isi kan langsung value nya
        KaryawanKontrak knrak = new KaryawanKontrak("Rizky Dwi", 25, anak, 100000, 10); // membuat objek ttp
        KaryawanTetap ttp = new KaryawanTetap("Joko", 45, anak, pokok);

        ttp.cetak();
        ttp.TotalGajiPokok();

        knrak.cetak(); // memanggil method cetak()
        knrak.Totalupah(); // memanggil method Totalupah dari class KaryawanKontrak
    }
}
