public class KaryawanTetap extends Karyawan {

 int gajiPokok;
 int Totalgaji;
 int tnjAnak;

 public KaryawanTetap(String nama, int umur, int tunjanganAnak, int gajiPokok) { // kontruktor dengan parameter
  super(nama, umur, tunjanganAnak); // keyword super sebgai pewarisan dari parent
  this.gajiPokok = gajiPokok;
 }

 public void TotalGajiPokok() {

  Totalgaji = this.gajiPokok + super.tunjanganAnak;
  System.out.println("Karyawan Tetap");
  System.out.println("Total Gaji Tetap: " + Totalgaji);
 }

 @Override
 public void cetak() {
  super.cetak();
 }

}
