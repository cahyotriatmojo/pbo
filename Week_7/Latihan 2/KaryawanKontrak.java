public class KaryawanKontrak extends Karyawan {

 int upah_Harian;
 int jmlhHadir;
 int Totalupah;

 public KaryawanKontrak(String nama, int umur, int tunjanganAnak, int upah_Harian, int jmlhHadir) {
  super(nama, umur, tunjanganAnak);
  this.upah_Harian = upah_Harian;
  this.jmlhHadir = jmlhHadir;
 }

 public void Totalupah() {
  Totalupah = (upah_Harian * jmlhHadir) + super.tunjanganAnak;

  System.out.println("Karyawan Kontrak");
  System.out.println("Total Gaji Kontrak: " + Totalupah);
 }

 @Override // dibuat saat kita ingin membuat ulang method pada sub-class..cetak ini dari
           // parent
 public void cetak() {
  super.cetak();
  System.out.println("Upah Harian: " + upah_Harian);
  System.out.println("Kehadiran: " + jmlhHadir);
 }

}
