class MountainBike extends Bicycle {  //turunan dari class Bicycle

    int seatHeight;

    public void setHeight(int newValue){  //membuat method void dgn parameter
        seatHeight = newValue;
        System.out.println("Seat Height:" + seatHeight);
    }

}

/**
 * MaountainBikeDemo *
 **/
class MountainBikeDemo {
     public static void main(String[] args) {

      MountainBike bikeg = new MountainBike(); //membuat objek

      bikeg.speedUp(20);
      bikeg.changeGear(7);  //memanggil method di objek
      bikeg.setHeight(25);

     }




}
