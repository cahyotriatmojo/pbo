import id.ac.dinus.lib.MayLib;  ///Langsung dari class
import id.ac.dinus.lib.YourLib;
import id.ac.dinus.test.HisLib; ///Mengakses semua kelas yang kita buat di package test

public class Aksilib2 {
   public static void main(String[] a)
   {
     MayLib m = new MayLib();   //buat objek m dari class MayLib
     m.cetak();  //memanggil method dari objek yang dibuat m

     YourLib y = new YourLib();
     y.cetak2();

     HisLib h = new HisLib();
     h.cetak3();
   }

}
